Fast Supervised Hashing with Decision Trees for High-Dimensional Data
=====================================================================

- This is the code for the papers [Fast Supervised Hashing with Decision Trees for High-Dimensional Data, IEEE Conf. Computer Vision and Pattern Recognition, 2014 (CVPR2014)](https://bytebucket.org/chhshen/fasthash/raw/466a3ef6d8c0b369e5324684baa7d6ca33f7e802/CVPR2014.pdf); and 
[Supervised Hashing Using Graph Cuts and Boosted Decision Trees, IEEE Transactions on Pattern Analysis and Machine Intelligence, 2015](http://arxiv.org/abs/1408.5574)

- [Download this repository](https://bitbucket.org/chhshen/fasthash/downloads)

- If you use this code in your research, please cite our papers:

``` 
 @article{FastHash2015Lin,
    author = {Guosheng Lin and Chunhua Shen and Anton {van den Hengel}},
    title  = {Supervised Hashing Using Graph Cuts and Boosted Decision Trees},
    journal= {IEEE Transactions on Pattern Analysis and Machine Intelligence},
    volume = {},
    number = {},
    year   = {2015},
    url    = {http://arxiv.org/abs/1408.5574},
    month  = {},
    pages  = {},
}
```

```
 @inproceedings{CVPR2014Lin,
   author    = "Guosheng Lin and  Chunhua Shen and  Qinfeng Shi and Anton {van den Hengel} and David Suter",
   title     = "Fast Supervised Hashing with Decision Trees for High-Dimensional Data",
   booktitle = "Proc. IEEE Conf. Computer Vision and Pattern Recognition",
   year      = "2014",
 }
```



## Install

Several toolboxes are required for running this code. For convenience, they are included in the folder: `/libs`
 and pre-compiled in Linux. These toolboxes are as follows:

- GraphCut is required for binary code inference, we use the implementation at <http://www.csd.uwo.ca/~olga/OldCode.html>, and the Matlab wrapper written by Brian Fulkerson.

- For fast decision tree training, we use the implementation from [Piotr's Image & Video Matlab Toolbox](http://vision.ucsd.edu/~pdollar/toolbox/doc/).


The implementation of two-step hashing (TSH) is also included here. 
Different types of loss functions and hash functions (e.g., linear SVM) are included in this code. 
Please refer to the description of TSH for details: <https://bitbucket.org/guosheng/two-step-hashing>.
See the TSH paper for details:
```
@inproceedings{ICCV13Lin,
   author    = "Guosheng Lin and  Chunhua Shen and  David Suter and  Anton {van den Hengel}",
   title     = "A general two-step approach to learning-based hashing",
   booktitle = "IEEE Conference on Computer Vision (ICCV'13)",
   address   = "Sydney, Australia",
   year      = "2013",
 }
```

The file `demo.m` shows that how to use the code.
Kindly note that the setting of tree depth should be adapted to datasets.



## Copyright

Copyright (c) Guosheng Lin, Chunhua Shen. 2014.

** This code is for non-commercial purposes only. For commerical purposes,
please contact Chunhua Shen <chhshen@gmail.com> **

This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.